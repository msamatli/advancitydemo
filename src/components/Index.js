import React from "react";

export default class Index extends React.Component {

    constructor(){
        super();

        this.state = {
            isLogin:false,
        }

        if(this.state.isLogin){
            this.props.history.push('/app');
        }
    }

  render() {
    return(
        <div>
           {this.props.children}
        </div>
    )
  }
}
