import React from "react";
import Header from "./shared/Header";
import LeftMenu from "./shared/LeftMenu";
import Dashboard from "./dashboard/Dashboard";
import Customer from "./customer/Customer";
import Content from "./Content";
import {  BrowserRouter, Route, Switch  } from 'react-router-dom';
import { connect } from "react-redux";

@connect((store) => {
    return {
      user: store.user.user,
      isUserFetched: store.user.fetched,
    };
  })
export default class Layout extends React.Component {

    componentWillMount() {
        if(!this.props.user.isLogin)
        {
            this.props.history.push('/');
        }
        else{
            console.log('success');
        }
    }

    render(){
        return(
                <div>
                    <Header />
                    <div className="row mr-0">
                            <div className="col-md-3">
                                <LeftMenu />
                            </div>
                            <div className="col-md-9">
                                <Switch>
                                    <Route exact path="/app" component={Dashboard}></Route>
                                    <Route path="/app/dashboard" component={Dashboard}></Route>
                                    <Route path="/app/customer" component={Customer}></Route>
                                </Switch>
                            </div>
                    </div>
                </div>
        );
    }
}