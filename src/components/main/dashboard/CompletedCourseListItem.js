import React from "react";
import "../../../assets/css/completedcourselistitem.css";

export default class CompletedCourses extends React.Component {

    constructor(){
        super();
    }

    render(){
        const {course} = this.props;

        return(
            <div className="row">
                <div className="col-md-1 col-md-1-narrow">
                    <span className="course-list-month">JUL</span><br/>
                    <span className="course-list-day">12</span>
                </div>
                <div className="col-md-9">
                    <span className="course-list-title">{course.title}</span><br/>
                    <span className="course-list-status">{course.status}</span>
                </div>
                <div className="col-md-2 col-md-2-expand text-right">
                    <span className="course-list-currency">+ {course.studentCount}</span><br/>
                    <span className="course-list-currency2">TL</span>
                </div>
            </div>
        );
    }
}