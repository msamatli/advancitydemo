import React from "react";
import CompletedCourseLıstItem from './CompletedCourseListItem';
import { Link  } from "react-router-dom";
import "../../../assets/css/completedcourses.css";

export default class CompletedCourses extends React.Component {

    constructor(){
        super();
    }

    render(){
        const CourseList = [{
            title: "The Basics Of Western Astrology Explained",
            courseDate: "01 January 2019",
            studentCount: 125,
            avgRating: 4,
            pictureUrl: "/assets/images/pic.png",
            completedPercentage: 30,
            status:'Payment Verified'
        },
        {
            title: "Compatible Inkjet Cartridge",
            courseDate: "10 January 2019",
            studentCount: 15,
            avgRating: 2,
            pictureUrl: "/assets/images/pic2.png",
            completedPercentage: 56,
            status:'Waiting'
        },
        {
            title: "Which Grill Should You Buy Charcoal Or Gas",
            courseDate: "01 February 2019",
            studentCount: 45,
            avgRating: 3,
            pictureUrl: "/assets/images/pic.png",
            completedPercentage: 43,
            status:'Not Completed'
        },
        {
            title: "Which Grill Should You Buy Charcoal Or Gas 2",
            courseDate: "15 January 2019",
            studentCount: 45,
            avgRating: 3,
            pictureUrl: "/assets/images/pic2.png",
            completedPercentage: 43,
            status:'Payment Waiting'
        }];

        const { userCode } = this.props;

        const courseLists = CourseList.map((course) => { return (<CompletedCourseLıstItem key={course.title} course={course}></CompletedCourseLıstItem>) });

        return(
            <div className="courses">
                <div className="row">
                    <div className="col-md-10"><h2 className="title">Completed</h2></div>
                    <div className="col-md-2 text-right"><Link to="#">See All --></Link></div>
                </div>
                {courseLists}
            </div>
        );
    }
}