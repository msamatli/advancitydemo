import React from "react";
import '../../../assets/css/dashboard.css';
import OngoingCourses from './OngoingCourses';
import CompletedCourses from './CompletedCourses';


export default class Dashboard extends React.Component {

    constructor(){
        super();

        

    }

   

    render(){
        let userCode = 38;
        return(
            <div>
                <h1 className="title">Dashboard</h1>
                <div class="ongoing-course-list">
                    <OngoingCourses userCode={userCode}/>
                </div>

                <div class="completed-course-list">
                    <CompletedCourses userCode={userCode}/>
                </div>
                
            </div>
        );
    }
}