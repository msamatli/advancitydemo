import React from "react";
import "../../../assets/css/coursecard.css";
import RatingStar from '../shared/RatingStar';
export default class CourseCard extends React.Component {

    constructor(){
        super();
    }

    render(){
        const { course } = this.props;
        let divStyle = {
            width:course.completedPercentage+'%',
            height:'2px',
        };
        return(
            <div className="course-card">
                <div className="">
                    <div className="col-md-6 pt-10 course-card-left">
                        <h6 className="course-title">{course.title}</h6>
                        <div className="course-card-left-bottom">
                            <span className="course-date">{course.courseDate}</span><br/>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style={divStyle} aria-valuenow={course.completedPercentage} aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span className="course-student">{course.studentCount} Students</span><br/>
                            <RatingStar rating={course.avgRating} />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <img className="course-image" src={course.pictureUrl}></img>
                    </div>
                </div>
            </div>
        );
    }
}