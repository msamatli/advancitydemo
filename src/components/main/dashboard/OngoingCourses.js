import React from "react";
import CourseCard from "./CourseCard";
import "../../../assets/css/ongoingcourselist.css";
import OwlCarousel from 'react-owl-carousel2';
import "../../../assets/css/owl.styles.css";

export default class OngoingCourses extends React.Component {

    constructor() {
        super();


    }

    render() {
        const CourseList = [{
            title: "The Basics Of Western Astrology Explained",
            courseDate: "01 January 2019",
            studentCount: 125,
            avgRating: 4,
            pictureUrl: "/assets/images/pic.png",
            completedPercentage: 30,
        },
        {
            title: "Compatible Inkjet Cartridge",
            courseDate: "10 January 2019",
            studentCount: 15,
            avgRating: 2,
            pictureUrl: "/assets/images/pic2.png",
            completedPercentage: 56,
        },
        {
            title: "Which Grill Should You Buy Charcoal Or Gas",
            courseDate: "01 February 2019",
            studentCount: 45,
            avgRating: 3,
            pictureUrl: "/assets/images/pic.png",
            completedPercentage: 43,
        },
        {
            title: "Which Grill Should You Buy Charcoal Or Gas 2",
            courseDate: "15 January 2019",
            studentCount: 45,
            avgRating: 3,
            pictureUrl: "/assets/images/pic2.png",
            completedPercentage: 43,
        }];

        const { userCode } = this.props;

        const courseCards = CourseList.map((course) => { return (<CourseCard key={course.title} course={course}></CourseCard>) });

        const options = {
            items:3,
            nav: false,
            rewind: true,
            autoplay: false,
            itemElement:"CourseCard",
            loop:true,
            autoWidth:true,
            dots:false,
        };
         
        const events = {

        };

        return (
            <div>
                <div className="course-list">
                <OwlCarousel ref="car" options={options} events={events} >
                    {courseCards}
                </OwlCarousel>
                </div>
            </div>

        );
    }
}