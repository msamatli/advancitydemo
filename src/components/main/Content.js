import React from "react";

export default class Content extends React.Component {

    constructor(){
        super();

        this.state = {
            isLogin:false,
        }
    }

  render() {
    return(
        <div>
           {this.props.children}
        </div>
    )
  }
}
