import React from "react";
import '../../../assets/css/header.css';
import { Link , NavLink } from "react-router-dom";
import { Navbar,Nav,NavItem,NavDropdown,MenuItem,Modal,Button } from 'react-bootstrap';
import SettingsCompact from "./SettingsCompact";

export default class Header extends React.Component {

    constructor() {
        super();
    
        this.handleHide = this.handleHide.bind(this);

        this.state = {
            showSettingsModal: false
        };
      }

      handleHide() {
        this.setState({ showSettingsModal: false });
      }

    render(){
        return(
            <div>
                <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <div className="logo">
                            <img src="/assets/images/icon.png"
                                    srcSet="/assets/images/icon@2x.png 2x,
                                    /assets/images/icon@3x.png 3x"></img>
                            <Link to="/app" className="logo-link"> ALMS</Link>
                        </div>
                        
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse className="container">
                    <div className="navbar-links">
                        <Nav>
                            <NavItem eventKey={1} href="#">
                                LESSONS
                            </NavItem>
                            <NavItem eventKey={2} href="#">
                                ACTIVITY
                            </NavItem>
                            <NavItem eventKey={3} href="#">
                                TASKS
                            </NavItem>
                            <NavItem eventKey={4} href="#" className="mobile-menu-link" >
                                USER PROFILE
                            </NavItem>
                        </Nav>
                    </div>
                    
                    <Nav pullRight className="user-profile-link">
                    <NavItem eventKey={4} href="#" className="hidden-xs" >
                        <div class="user-profile" onClick={() => this.setState({ showSettingsModal: true })}>
                            <div>
                            <img src="/assets/images/60-px.png"
                                srcSet="/assets/images/60-px@2x.png 2x,
                                    /assets/images/60-px@3x.png 3x"
                                class="px2"></img>
                                <img className="ml-10" src="/assets/images/dropdown-arrow.svg" ></img>
                            </div>
                        </div>
                    </NavItem>
                    </Nav>
                </Navbar.Collapse>
                </Navbar>
                <Modal
                    show={this.state.showSettingsModal}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="modal-title"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="modal-title">
                        Settings
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <SettingsCompact></SettingsCompact>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleHide}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
            
                );
    }
}