import React from "react";

export default class RatingStar extends React.Component {

    constructor(){
        super();
    }

  render() {
    const rating = this.props.rating;

    let stars = [];
    for (let index = 0; index < 5; index++) {
        if (index < rating) {
            stars.push(<i className="fas fa-star yellow" key={index}></i>);
        }
        else{
            stars.push(<i className="far fa-star" key={index}></i>);
        }
    }

    return (<div>{stars}</div>);
  }
}
