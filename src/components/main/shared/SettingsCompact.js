import React from "react";
import "../../../assets/css/settingscompact.css";
import { Tabs,Tab } from 'react-bootstrap';

export default class SettingsCompact extends React.Component {

    constructor(){
        super();
    }

    render(){
       const { course } = this.props;
        return(
            <div className="settings-compact">
                <Tabs defaultActiveKey={1}>
                    <Tab eventKey={1} title="General" id="tab1" className="tabContent">
                        <div className="row">
                        <div className="col-md-12 col-xs-12"><span className="bold">Who can see your friends list?</span></div>
                            
                        </div>
                        <hr></hr>
                        <div className="row">
                            <div className="col-md-12 col-xs-12"><span className="bold">Link your profile by search engines outside</span></div>
                            
                        </div>
                        <hr></hr>
                        <div className="row">
                            <div className="col-md-12 col-xs-12"><span className="bold">Limit the audience for old posts on your timeline</span></div>
                        
                        </div>
                        <hr></hr>
                        <div className="row">
                            <div className="col-md-6 col-xs-6">
                                <span className="bold">Who can look you up using the phone number you provided?</span>
                            </div>
                            <div className="col-md-6 col-xs-6">
                                <span className="bold">Who can look you up using the email address you provided?</span>
                            </div>
                        </div>
                    </Tab>
                    <Tab eventKey={2} title="Data" id="tab2" className="tabContent">
                        2    
                    </Tab>
                    <Tab eventKey={3} title="Privacy" id="tab3" className="tabContent">
                        3
                    </Tab>
                    <Tab eventKey={4} title="Other" id="tab4" className="tabContent">
                        4
                    </Tab>
                </Tabs>
            </div>
        );
    }
}