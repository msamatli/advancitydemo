import React from "react";
import "../../../assets/css/weeklycard.css";
import {BarChart} from 'react-easy-chart';

export default class WeeklyPerformance extends React.Component {

    constructor(){
        super();
    }

  render() {
    const data = [
        { x: "Mo",  y: 60  },
        { x: "Tu", y: 45  },
        { x: "We", y: 52  },
        { x: "Th",  y: 28  },
        { x: "Fr",  y: 10  },
        { x: "Sa",  y: 34  },
        { x: "Su",  y: 17  }];
    let graph = [];
    for (let index = 0; index < data.length; index++) {
        let element = data[index];
        let divStyle = {
            backgroundColor: 'blue',
            width:'25px',
            height: (element.y)+'px',
            borderRadius:'2px',
            margin:'0 auto',
          };
        graph.push(<div className="graph-column p-0" key={index}>
                        <div style={divStyle}>

                        </div>
                        <p className="text-center">{element.x}</p>
                    </div>);
    }
    return (
        <div className="weekly-card">
            <div className="row">
                <h6 className="pull-left weekly-title">WEEKLY PERFORMANCE</h6>
                <img className="ml-10 mt-14 pull-right" src="/assets/images/dropdown-arrow.svg"></img>
                <h6 className="pull-right weekly-combo">Last 7 Days</h6>
            </div>
            <div className="row graph-title-content">
                <div className="pull-left">
                    <span className="graph-title">Interaction</span><br/>
                    <span className="graph-subtitle">Total taps on your posts or profile</span>
                </div>
                <div className="pull-right">
                    <span className="graph-number">146</span>
                </div>
            </div>
            <div className="row">
                <div className="graph">
                    {graph}
                </div>
            </div>
        </div>);
  }
}
