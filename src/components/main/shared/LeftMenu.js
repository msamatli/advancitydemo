import React from "react";
import { Link , NavLink } from "react-router-dom";
import '../../../assets/css/leftmenu.css';
import WeeklyPerformance from "./WeeklyPerformance";

export default class LeftMenu extends React.Component {

    render(){
        return(
            <div className="left-menu">
                <div className="user-card">
                    <div className="row mb-20">
                        <div className="col-md-4">
                            <img src="/assets/images/60-px.png"
                                srcSet="/assets/images/60-px@2x.png 2x,
                                    /assets/images/60-px@3x.png 3x"
                                class="px"></img>
                        </div>
                        <div className="col-md-8 user-info">
                            <div className="">
                                <span className="user-name">Lena White</span><br/>
                                <span className="user-title">Software Developer</span>
                            </div>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="col-xs-4 rb">
                            <span className="stats-number">569</span><br/>
                            <span className="stats-name">LESSONS</span>
                        </div>
                        <div className="col-xs-4 rb">
                            <span className="stats-number">89</span><br/>
                            <span className="stats-name">SAMPLE</span>
                        </div>
                        <div className="col-xs-4">
                            <span className="stats-number">102</span><br/>
                            <span className="stats-name">FOLLOWING</span>
                        </div>
                    </div>
                    <hr />
                </div>
                <div>
                    <h6 className="ml-30">MAIN MENU</h6>
                    <div className="link-menu">
                        <NavLink to='/app/dashboard' className="link" activeClassName="link-active" ><img src="/assets/images/signal-1517.svg" className="link-icon"></img> Dashboard</NavLink>
                        <NavLink to='/app/customer' className="link" activeClassName="link-active" ><img src="/assets/images/profile-round-1342.svg" className="link-icon"></img> Customers</NavLink>
                        <NavLink to='/app/alerts' className="link" activeClassName="link-active" ><img src="/assets/images/megaphone-775.svg" className="link-icon"></img> Alerts</NavLink>
                        <NavLink to='/app/calendar' className="link" activeClassName="link-active" ><img src="/assets/images/calendar-1197.svg" className="link-icon"></img> Calendar</NavLink>
                        <NavLink to='/app/inbox' className="link" activeClassName="link-active" ><img src="/assets/images/inbox-paper-round-1556.svg" className="link-icon"></img> Inbox</NavLink>
                        <NavLink to='/app/memos' className="link" activeClassName="link-active" ><img src="/assets/images/edit-1482.svg" className="link-icon"></img> Memos</NavLink>
                    </div>
                    
                </div>
                <div>
                    <WeeklyPerformance />
                    <hr />
                </div>
            </div>

        );
    }
}