import React from "react";
import '../../assets/css/login.css';
import FormValidator from '../../utils/FormValidator';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { fetchUser,userLogin } from "../../actions/userActions";
import SweetAlert from 'sweetalert2-react';

@connect((store) => {
    return {
      user: store.user.user,
      isUserFetched: store.user.fetched,
    };
  })
export default class Login extends React.Component {

    constructor(){
        super();

        this.validator = new FormValidator([
            { 
              field: 'email', 
              method: 'isEmpty', 
              validWhen: false, 
              message: 'Email is required.' 
            },
            { 
              field: 'email',
              method: 'isEmail', 
              validWhen: true, 
              message: 'That is not a valid email.'
            },
            { 
              field: 'password', 
              method: 'isEmpty', 
              validWhen: false, 
              message: 'Password is required.'
            }
          ]);

          this.state = {
            email: '',
            password: '',
            validation: this.validator.valid(),
            isPasswordVisible: false,
            showAlert:false,
          }
      
          this.submitted = false;
    }

    handleInputChange = event => {
        event.preventDefault();
    
        this.setState({
          [event.target.name]: event.target.value,
        });

        const validation = this.validator.validate({
            [event.target.name]: event.target.value,
          });

        this.setState({'validation':{
            ...this.state.validation, 
            [event.target.name] :validation[event.target.name]
        }});
        
        
      }

      handleFormSubmit = event => {
        event.preventDefault();
    
        const validation = this.validator.validateForm(this.state);
        this.setState({ validation });
        this.submitted = true;
    
        if (validation.isValid) {
            this.setState({isLogin:true});
            if(this.state.email == this.props.user.email 
                && this.state.password == this.props.user.password ){
                    this.props.dispatch(userLogin('SADJSADKSADJASKD'));
                    this.props.history.push('/app');
            }
            else{
                this.setState({showAlert:true});
            }
        }
      }

      togglePasswordVisibility = event => {
          this.setState({isPasswordVisible:!this.state.isPasswordVisible});
      }

      componentWillMount() {
        this.props.dispatch(fetchUser())
      }

  render() {
   
    const { user } = this.props;
    
    let validation = this.submitted ?                       
                        this.validator.validate(this.state) :   
                        this.state.validation ;               


    return (
        <div className="container">
            <div className="base">
                <div className="login-panel">
                    <div className="col-md-6 login-left">
                        <div className="left-container">
                            <img src="../../assets/images/icon.png"
                                srcSet="../../assets/images/icon@2x.png 2x,
                                ../../assets/images/icon@3x.png 3x"
                                class="icon"></img>
                            <h2 className="sign-in"> Sign In </h2>
                            <div className="input-container">
                                <label>E-MAIL ADDRESS</label>
                                <div className={"input-base "+ (validation.email.isInvalid && 'has-error')}>
                                    <input type="email" name="email" className="form-control" onChange={this.handleInputChange.bind(this)} autoComplete="off"></input>
                                    <span className="help-block">{validation.email.message}</span>
                                </div>
                            </div>
                            <div className="input-container">
                                <label>PASSWORD</label>
                                <div className={"col-md-12 input-base " + (validation.password.isInvalid && 'has-error')}>
                                    {
                                        this.state.isPasswordVisible?
                                        <input type="text" name="password" className="form-control password-input" onChange={this.handleInputChange.bind(this)}></input>:
                                        <input type="password" name="password" className="form-control password-input" onChange={this.handleInputChange.bind(this)}></input>
                                    }
                                    <span  onClick={() => this.setState({ isPasswordVisible: !this.state.isPasswordVisible })}  
                                        className="view-password"><i class="far fa-eye"></i></span>
                                    <span className="help-block">{validation.password.message}</span>
                                </div>
                            </div> 
                            
                            <div className="form-group">
                                <button onClick={this.handleFormSubmit.bind(this)} type="button" class="btn btn-login btn-md">Sign In</button>
                            </div>
                            <div className="or-connect-with">
                                <label> or connect with </label>
                            </div>
                        </div>
                        <div className="left-bottom">
                            <div className="social-buttons">
                                <a href="#" title="Twitter" className="btn btn-twitter btn-lg"><i className="fab fa-twitter mr-10"></i> Twitter</a>
                                <a href="#" title="Facebook" className="btn btn-facebook btn-lg"><i className="fab fa-facebook-f mr-10"></i> Facebook</a>
                                <a href="#" title="Google" className="btn btn-google btn-lg"><i className="fab fa-google mr-10"></i> Google</a>
                            </div>
                        </div>
                        
                    </div>
                    <div className="col-md-6 login-right hidden-sm hidden-xs">
                        <img src="../../assets/images/pic-base.png"
                            srcSet="../../assets/images/pic-base@2x.png 2x,
                            ../../assets/images/pic-base@3x.png 3x" />
                    </div>
                </div>
                <div className="container center-sm">
                    <div className="col-md-10 ">
                        <a href="#" className="forget-your-password pull-right">
                            Forget your password?
                        </a>
                    </div>
                </div>
                <SweetAlert
                    show={this.state.showAlert}
                    title="Wrong User/Password"
                    text="For demo access use a@b.com/pass"
                    onConfirm={() => this.setState({ showAlert: false })}
                />
            </div>
        </div>
    );
  }
}
