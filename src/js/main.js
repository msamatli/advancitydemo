import React from "react";
import ReactDOM from "react-dom";
import {  BrowserRouter as Router, Route, Switch  } from 'react-router-dom';
//import { Router,Route,IndexRoute,hashHistory,Switch} from 'react-router';
import Index from "../components/Index";
import Login from "../components/auth/Login";
import Layout from "../components/main/Layout";
import { Provider } from "react-redux";
import store from "./store";

const app = document.getElementById('app');

// ReactDOM.render(
//     <BrowserRouter>
//         <Switch>
//             <Route exact path="/" component={Login}/>
//             <Route path="/dashboard" component={Layout}/>
//         </Switch>
//     </BrowserRouter>, app);


ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Index>
                <Switch>
                    <Route exact path="/" component={Login}></Route>
                    <Route path="/app" component={Layout}></Route>
                </Switch>
            </Index>
        </Router>
    </Provider>, app);
