export default function reducer(state={
    user: {
      email: null,
      password: null,
      isLogin:false,
      sessionCode:"",
    },
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "FETCH_USER_OFFLINE": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          user: action.payload,
        }
      }
      case "USER_LOGIN": {
        return {
          ...state,
          user: {...state.user, isLogin: action.payload.isLogin,sessionCode:action.payload.sessionCode},
        }
      }
    }

    return state
}
