export function fetchUser() {
    return {
      type: "FETCH_USER_OFFLINE",
      payload: {
        email:"a@b.com",
        password:"pass"
      }
    }
  }

  export function userLogin(sessionCode) {
    return {
      type: 'USER_LOGIN',
      payload: {
          isLogin:true,
          sessionCode:sessionCode
      },
    }
  }

  